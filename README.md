``` ______     __     ______     __  __     ______   __         ______     ______     ______       
/\  == \   /\ \   /\  ___\   /\ \_\ \   /\__  _\ /\ \       /\  __ \   /\  == \   /\  ___\      
\ \  __<   \ \ \  \ \ \__ \  \ \  __ \  \/_/\ \/ \ \ \____  \ \  __ \  \ \  __<   \ \___  \     
 \ \_\ \_\  \ \_\  \ \_____\  \ \_\ \_\    \ \_\  \ \_____\  \ \_\ \_\  \ \_____\  \/\_____\    
  \/_/ /_/   \/_/   \/_____/   \/_/\/_/     \/_/   \/_____/   \/_/\/_/   \/_____/   \/_____/    
                                                                                                
 __     __   __     ______   ______     ______     __   __   __     ______     __     __        
/\ \   /\ "-.\ \   /\__  _\ /\  ___\   /\  == \   /\ \ / /  /\ \   /\  ___\   /\ \  _ \ \       
\ \ \  \ \ \-.  \  \/_/\ \/ \ \  __\   \ \  __<   \ \ \'/   \ \ \  \ \  __\   \ \ \/ ".\ \      
 \ \_\  \ \_\\"\_\    \ \_\  \ \_____\  \ \_\ \_\  \ \__|    \ \_\  \ \_____\  \ \__/".~\_\     
  \/_/   \/_/ \/_/     \/_/   \/_____/   \/_/ /_/   \/_/      \/_/   \/_____/   \/_/   \/_/     
                                                                                                
 ______   ______     ______     ______                                                          
/\__  _\ /\  ___\   /\  ___\   /\__  _\                                                         
\/_/\ \/ \ \  __\   \ \___  \  \/_/\ \/                                                         
   \ \_\  \ \_____\  \/\_____\    \ \_\                                                         
    \/_/   \/_____/   \/_____/     \/_/   

```

## Tasks

* [ ] Add lazy loading to the announcementsComponent@loadAnnouncements - Create a directive that will checking the size of the container where you have the announcements list. if it's reaching the end of the div container, do a callback function that will trigger @loadAnnouncements

* [ ] Add search capability for the announcements list

* [ ] Add funtionality to the refresh button to load the announcements list ( if you have a searchText value, it has to be kept in the query as well)

* [ ] When lazy loading is implemented, search bar has to be fixed and the announcements list is scrollable only

* [ ] Add a function on the "+" ( add new announcement ) that will redirect you to app.announcement with :id = 'new'

### announcementCardComponent

* [ ] When you click on the announcement title, you will also redirect to the app.announcement with :id = {{announcement.announcementId}}

* [ ] Display the participants count in the announcementCardComponent

* [ ] Enforce one-time binding for [title, content, updated_at, participantsCount]


### AnnouncementSetupComponent

* [ ] Create all the logic for the Create | Upate | Delete Announcement - this will involve knoledge of forms, services, and http calls to the backend

  #### small tasks description 
  
* [ ] Create a form for the title and content ( add validations to the title, and content )

* [ ] create a sub form for the participants lists ( you are free to do a list or a table or chips to display all the participants and multi select them) - add validations ( list must contain at least one participant)

* [ ] Add lazy loading for the participants list ( load 20 by 20 ) ( if you decided to use chips, they have to also be loaded asyc.)

Notes: 

To save an announcement you must save first the announcement and then the participants linkage ( you will have to do nested promises )

To update an announcement, you have to verify that something has changed in the form, otherwise, you won't send an api to the backend, you will just redirect back to the announcements list

The buttons at the end of the form : Delete ( only if you have an ID ) Save ( disable if validations didn't pass ) and Cancel ( this will redirect back to the list )


### Bonus

* [ ] Change the announcements.service to use the http.service.js instead of calling directly the $http angularJS service. 

* [ ] Add validations to the http.service.js for all the verbs.

* [ ] Make it fully responsive for Mobile devices ( iphone 5 as minimum - 320px width )

### Notes

For the CSS - you can create your own classes but extra points if you follow 100% the material design patterns.

our main file for the AngularJS application is inside resources->views->app.php

In order for you to understand which params to pass to each controller in Laravel, you can inspect the folder  app->Http->Controllers

In order for you to understand how the routes are written in Laravel, you can inspect the folder  routes->api.php and routes->web.php

All the AngularJS files are inside public->frontend