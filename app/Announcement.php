<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
   
    protected $fillable = [
        'title',
        'content'
    ];
    
    protected $table = "announcements";
    
    protected $primaryKey = "announcementId";
    
    public function participants() {
        return $this->belongsToMany(Participant::class, 'announcement_participant', 'announcementId', 'participantId');
    }

    public function syncParticipants($participantIds = []) {
        
        $this->participants()->sync($participantIds);
        
        return $this->participants()->get();
        
    }

}
