<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Announcement;

use Exception;

use App\Http\Requests\AnnouncementRequest;

class AnnouncementsController extends Controller
{
    
    public function index(AnnouncementRequest $request) {
        
        $announcements = new Announcement();
        
        $announcements = $this->filtersAndRelationships($announcements, $request);
       
        if($request->page) {
                
            $announcements = $announcements->paginate($request->perPage ? $request->perPage : 20);    
                
        } else {
            
            $announcements = $announcements->get();
        }
        
        
        
        return $announcements;
    }
        
    
    private function filtersAndRelationships($announcements, $request) {
        
        
        if($request->searchText) {
            
            $announcements = $announcements->where("title", "LIKE", "%". $request->searchText ."%");
        
            
        }
        
        if($request->withParticipants) {
            
            $announcements = $announcements->with('participants');
        
            
        }
        
        return $announcements;
    }
        
    public function show(AnnouncementRequest $request, $announcementId) {
        
        $announcement = new Announcement();
        
        $announcement = $this->filtersAndRelationships($announcement, $request);

        $announcement = $announcement->find($announcementId);
        
        if (!$announcement) {
            
           return response()->json(['error' => 'Announcement not found'], 404);
            
        }
        
        return $announcement;
        
    }
    
    public function store(AnnouncementRequest $request) {
    
    
        $params = $request->all();
        
        $newAnnouncement = new Announcement();
        
        $newAnnouncement->fill($params);
        
        $newAnnouncement->save();
        
        return $newAnnouncement;
    
    }
    
    public function update(AnnouncementRequest $request, $announcementId) {
                
        $announcement = new Announcement();
        
        $announcement = $announcement->find($announcementId);
        
        if (!$announcement) {
            
            return response()->json(['error' => 'Announcement not found'], 404);
             
        }
        
        $announcement->fill($request->all());
        
        $announcement->save();
        
        return $announcement;
    }
    
    public function destroy(AnnouncementRequest $request, $announcementId) {
                
        $announcement = new Announcement();
        
        $announcement = $announcement->find($announcementId);
        
        if (!$announcement) {
            
            return response()->json(['error' => 'Announcement not found'], 404);
            
        }
        
        $announcement->delete();
        
        return $announcement;
    }
    
    public function attachParticipants(AnnouncementRequest $request, $announcementId) {
        
        $announcement = new Announcement();
        
        $announcement = $announcement->find($announcementId);
        
        if (!$announcement) {
            
            return response()->json(['error' => 'Announcement not found'], 404);
            
        }
        
        try {
            
            $announcement->participants = $announcement->syncParticipants($request->participantIds);
        
            
        } catch(Exception $exception) {
            
             return response()->json(['error' => $exception->getMessage()], 500);
             
        }
        
        return $announcement;
    }
    
}
