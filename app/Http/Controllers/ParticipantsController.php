<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Participant;

class ParticipantsController extends Controller
{
    
    public function index(Request $request) {
        
        $participants = new Participant();
        
        if ($request->searchText) {
            
           $participants = $participants->where(function($query) use ($request) {
                
                $query = $query->where('firstName', 'LIKE',  '%'. $request->searchText .'%');
                
                $query = $query->orWhere('lastName', 'LIKE',  '%'. $request->searchText .'%');
                   
                $query = $query->orWhere('email', 'LIKE',  '%'. $request->searchText .'%');
                
           });
            
        }
        
        if ($request->page) {
        
            $participants = $participants->paginage(20);
            
        } else {
            
            $participants = $participants->get();
            
        }
        
        
        return $participants;
    }
    
}
