<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    //
    protected $fillable = [
        'firstName',
        'lastName',
        'email'
    ];
    
    protected $table = "participants";
    
    protected $primaryKey = "participantId";
    
    public function announcements() {
        return $this->belongsToMany(Announcement::class, 'announcement_participant', 'participantId', 'announcementId');
    }
}
