<?php

use Illuminate\Database\Seeder;

use App\Announcement;

use App\Participant;

use Faker\Factory as Faker;

class AnnouncementsSeeder extends Seeder
{
    /**
     * Run the database seeds for Announcements and announcement_participant pivot table.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();
        
        $participants = Participant::get();
        
        $participantIds = array_pluck($participants, 'participantId');
        
        foreach (range(1,50) as $index) {
	        
	        $announcement = new Announcement();
	        
	        $announcement->fill([
	            'title' => $faker->sentence,
	            'content' => $faker->paragraph
	        ]);
	           
	       $announcement->save();
	       
	       $announcement->participants()->attach($faker->randomElements($participantIds, 10));
            
        }
        
    }
}
