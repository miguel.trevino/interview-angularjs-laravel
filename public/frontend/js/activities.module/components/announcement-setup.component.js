

var announcementSetupComponent = {
    templateUrl: 'frontend/templates/activities.module/announcement-setup.component.html',
    controller: announcementSetupController,
    controllerAs: 'announcementSetupCtrl'
};

announcementSetupController.$inject = [];

function announcementSetupController() {
    
    var vm = this;
    
    
    vm.$onInit = function() {
        
        console.log('announcementSetupController loaded');
        
    };
    
    
}
