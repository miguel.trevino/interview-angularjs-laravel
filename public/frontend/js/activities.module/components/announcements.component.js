

var announcementsComponent = {
    templateUrl: 'frontend/templates/activities.module/announcements.component.html',
    controller: announcementsController,
    controllerAs: 'announcementsCtrl'
};

announcementsController.$inject = ["$http", "announcementService"];

function announcementsController($http, announcementService) {
    
    var vm = this;
    
    vm.searchText = '';
        
    vm.announcements = [
        {
            title: 'Announcement 1',
            updated_at: new Date(),
            participantsCount: 10
        },
        {
            title: 'Announcement 2',
            updated_at: new Date(),
            participantsCount: 20
        },
        {
            title: 'Announcement 3',
            updated_at: new Date(),
            participantsCount: 30
        }    
    ];
    
    
    vm.loadAnnouncements = loadAnnouncements;
    
    vm.$onInit = function() {
        
        console.log('announcementsComponent loaded');
      
        loadAnnouncements();
    };
    
    
    function loadAnnouncements() {
        
        var data = {};
        
        var request = announcementService.requestAnnouncements(data);

        request.then(function (response) {

            vm.announcements = response.data;
            
        }, function (error) {

        });        
    }
    
}
