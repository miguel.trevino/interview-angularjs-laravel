var announcementService = ['$http', function ($http) {
    
    var vm = this;
    
    vm.url = "https://testinterview-miguel222tm.c9users.io/api/";
    
    var service = {
        requestAnnouncements: requestAnnouncements,
        requestAnnouncement: requestAnnouncement,
        createAnnouncement: createAnnouncement,
        updateAnnouncement: updateAnnouncement,
        deleteAnnouncement: deleteAnnouncement,
        attachParticipants: attachParticipants
    };
    
    function requestAnnouncements(params){
        return $http.get(vm.url + "announcements", params);
    }
    
    function requestAnnouncement(index){
        return $http.get(vm.url + "announcements/" + index);
    }
    
    function createAnnouncement(params){
        return $http.post(vm.url + "announcements", params);
    }
    
    function updateAnnouncement(index, params){
        return $http.put(vm.url + "announcements/" + index, params);
    }
    
    function deleteAnnouncement(index){
        return $http.delete(vm.url + "announcements/" + index);
    }
    
    function attachParticipants(index, params){
        return $http.put(vm.url + "announcements/"+ index +"/participants", params);
    }
    
    return service;
}];