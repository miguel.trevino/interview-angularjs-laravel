var participantService = ['$http', function ($http) {
    
    var vm = this;
    
    vm.url = "https://testinterview-miguel222tm.c9users.io/api/";
    
    var service = {
        requestParticipants: requestParticipants
    };

    function requestParticipants(params){
        return $http.get(vm.url + "participants", params);
    }
    
    return service;
}];