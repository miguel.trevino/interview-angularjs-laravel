

var app = angular.module('interview.module', [
    'ngRoute',
    'ui.router',
    'ngMaterial',
    'activities.module'
]);


app.controller('mainController', mainController);

app.service('httpService', httpService);

app.config(config);